package interfacor

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"unicode"
)

var ErrMethodNotExists error = errors.New("method does not exist in the interface")

// ExecMethod executes the method of the interface
// Arguments:
// - i interface{} : the interface that you want a method to be executed from
// - args []string : the method followed by the arguments that need to be passed to the method
func ExecMethod(iface interface{}, args []string) (retval []reflect.Value, err error) {
	defer func() {
		if x := recover(); x != nil {
			retval = nil
			err = fmt.Errorf("We panicked: %v", x)
		}
	}()

	method := reflect.ValueOf(iface).MethodByName(args[0])

	if !method.IsValid() {
		return nil, ErrMethodNotExists
	}

	// building the args
	if method.Type().NumIn() != len(args[1:]) {
		panic("arguments provided does not match arguments needed")
	}

	inputs := make([]reflect.Value, len(args)-1)
	for i := range args[1:] {
		inputs[i] = stringToKind(args[i+1], method.Type().In(i).Kind())
	}

	retvals := method.Call(inputs)

	var retError error
	var nonErrorRetvals []reflect.Value

	for _, v := range retvals {
		if v.Type().Implements(reflect.TypeOf((*error)(nil)).Elem()) {
			if !v.IsNil() {
				err := v.Interface().(error)
				if retError == nil {
					retError = err
				} else {
					panic("to many errors returned for interfacor to handle")
				}
			}
		} else {
			nonErrorRetvals = append(nonErrorRetvals, v)
		}
	}
	if retError != nil {
		return nil, retError
	}
	return nonErrorRetvals, nil
}

// GetMethods returns a serialized string of searched for methods in an interface
// Arguments
// - iface interface{} : the interface that you are inspecting
// - search string : string used to filter the output "" = all results
func GetMethods(iface interface{}, search string) []string {
	var methods []string
	ifaceType := reflect.TypeOf(iface).Elem()
	inputLower := strings.ToLower(search)
	for i := 0; i < ifaceType.NumMethod(); i++ {
		method := ifaceType.Method(i)
		if unicode.IsUpper(rune(method.Name[0])) {
			methodLower := strings.ToLower(method.Name)
			if strings.Contains(methodLower, inputLower) {
				methodName := method.Name + getMethodSignature(method)
				methods = append(methods, methodName)
			}
		}
	}
	return methods
}

func getMethodSignature(method reflect.Method) string {
	signature := "("
	for i := 0; i < method.Type.NumIn(); i++ {
		if i > 0 {
			signature += ", "
		}
		signature += method.Type.In(i).String()
	}
	signature += ")"

	retvalCount := method.Type.NumOut()

	if retvalCount > 0 {
		if retvalCount > 1 {
			signature += " ("
		} else {
			signature += " "
		}
		for i := 0; i < retvalCount; i++ {
			if i > 0 {
				signature += ", "
			}
			signature += method.Type.Out(i).String()
		}
		if retvalCount > 1 {
			signature += ")"
		}

	}
	return signature
}

func stringToKind(str string, k reflect.Kind) reflect.Value {
	switch k {
	case reflect.String:
		return reflect.ValueOf(str)

	case reflect.Int:
		intValue, _ := strconv.ParseInt(str, 10, 64)
		return reflect.ValueOf(int(intValue))
	case reflect.Int8:
		intValue, _ := strconv.ParseInt(str, 10, 8)
		return reflect.ValueOf(int8(intValue))
	case reflect.Int16:
		intValue, _ := strconv.ParseInt(str, 10, 16)
		return reflect.ValueOf(int16(intValue))
	case reflect.Int32:
		intValue, _ := strconv.ParseInt(str, 10, 32)
		return reflect.ValueOf(int32(intValue))
	case reflect.Int64:
		intValue, _ := strconv.ParseInt(str, 10, 64)
		return reflect.ValueOf(intValue)

	case reflect.Uint:
		uintValue, _ := strconv.ParseUint(str, 10, 64)
		return reflect.ValueOf(uint(uintValue))
	case reflect.Uint8:
		uintValue, _ := strconv.ParseUint(str, 10, 8)
		return reflect.ValueOf(uint8(uintValue))
	case reflect.Uint16:
		uintValue, _ := strconv.ParseUint(str, 10, 16)
		return reflect.ValueOf(uint16(uintValue))
	case reflect.Uint32:
		uintValue, _ := strconv.ParseUint(str, 10, 32)
		return reflect.ValueOf(uint32(uintValue))
	case reflect.Uint64:
		uintValue, _ := strconv.ParseUint(str, 10, 64)
		return reflect.ValueOf(uintValue)

	case reflect.Float32:
		floatValue, _ := strconv.ParseFloat(str, 32)
		return reflect.ValueOf(float32(floatValue))
	case reflect.Float64:
		floatValue, _ := strconv.ParseFloat(str, 64)
		return reflect.ValueOf(floatValue)

	case reflect.Bool:
		boolValue, _ := strconv.ParseBool(str)
		return reflect.ValueOf(boolValue)

	default:
		panic("unsupported type " + k.String())
	}
}
