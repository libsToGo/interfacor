# Interfacor

"Interfacor" offers functionality for dynamically invoking methods on interfaces and retrieving detailed information about these methods in Go. This package is particularly useful for applications that require interaction with interfaces through string-based inputs, such as command-line arguments, line protocol implementations, or rudimentary execution of methods specified in a file. The design and utility of "Interfacor" draw inspiration from the Python 'Fire' package, aiming to bring similar ease of use and dynamic capabilities to Go programming.

## Installation

```bash
go get gitlab.com/libsToGo/interfacor
```

## Terminal arguments example

A simple example of how to dynamically execute from command line

```go
package main

import (
	"fmt"
	"os"

	"gitlab.com/libsToGo/interfacor"
)

type Say interface {
	Hello(string) string
	Bey(string) string
}

type say struct {
}

func (s say) Hello(str string) string {
	return fmt.Sprint("Hello ", str, ", I hope all is well")
}

func (s say) Bey(str string) string {
	return fmt.Sprint("Bey ", str, ", have a good day")
}

func main() {
	var myInterface Say = &say{}

	command := os.Args[1:]

	resp, err := interfacor.ExecMethod(myInterface, command)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println(resp)

	}

}
```

```bash
$ go run . Hello world
[Hello world, I hope all is well]
$ go run . Bey world  
[Bey world, have a good day]
```

*Please see the examples for more use case examples*