package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/libsToGo/interfacor"
)

var (
	command  string
	commands MyInterface = &myStruct{}
)

func GetKey(f func(b byte)) {
	exec.Command("stty", "-F", "/dev/tty", "cbreak", "min", "1").Run()
	exec.Command("stty", "-F", "/dev/tty", "-echo").Run()

	var b []byte = make([]byte, 1)

	fmt.Print(">")
	for {
		os.Stdin.Read(b)
		f(b[0])
	}
}

func ProcKey(b byte) {
	switch {
	case b == 10:
		args := strings.Split(command, " ")
		resp, err := interfacor.ExecMethod(commands, args)

		fmt.Println()

		if err != nil {
			fmt.Println("err: ", err.Error())
			fmt.Println("The following methods are avaiable")

			for _, m := range interfacor.GetMethods(&commands, "E") {
				fmt.Println(m)
			}

		} else {
			for _, r := range resp {
				if len(r.String()) > 0 {
					fmt.Printf("%v\n", r)
				}
			}
		}
		command = ""
	case b >= 65 && b <= 122:
		command += string(b)
	case b >= 48 && b <= 57:
		command += string(b)
	case b == 32:
		command += " "
	case b == 127:
		if len(command) > 0 {
			command = command[:len(command)-1]
		}
	}

	fmt.Printf("\033[2K\r> %s", command)
}

func main() {
	GetKey(ProcKey)
}
