package main

type MyInterface interface {
	Set(str string)
	Get() string
	Echo(str string) string
}

type myStruct struct {
	val string
}

func (m *myStruct) Echo(str string) string {
	return str
}

func (m *myStruct) Set(str string) {
	m.val = str
}

func (m myStruct) Get() string {
	return m.val
}
