package main

import (
	"fmt"

	"gitlab.com/libsToGo/interfacor"
)

type MyInterface interface {
	Set(string)
	Get() string
}

type myStruct struct {
	val string
}

func (m *myStruct) Set(s string) {
	m.val = s

}
func (m *myStruct) Get() string {
	return m.val
}

func main() {
	var myInterface MyInterface
	myInterface = &myStruct{}

	// get an array of all the methods and their signatures
	methods := interfacor.GetMethods(&myInterface, "")
	fmt.Println("Print all methods")
	for _, m := range methods {
		fmt.Println(m)
	}

	// get an array of all the methods and their signatures where the method names includes <ge>
	methods = interfacor.GetMethods(&myInterface, "ge")
	fmt.Println("\nPrint all methods with the letters <ge> in them")
	for _, m := range methods {
		fmt.Println(m)
	}
}
