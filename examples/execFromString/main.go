package main

import (
	"fmt"

	"gitlab.com/libsToGo/interfacor"
)

type MyInterface interface {
	Set(string)
	Get() string
}

type myStruct struct {
	val string
}

func (m *myStruct) Set(s string) {
	m.val = s
}

func (m *myStruct) Get() string {
	return m.val
}

func main() {
	var myInterface MyInterface
	myInterface = &myStruct{}

	commands := [][]string{
		{"Set", "foobar"},
		{"Get"},
	}

	for _, command := range commands {
		fmt.Println("executing", command)

		resp, err := interfacor.ExecMethod(myInterface, command)
		if err != nil {
			panic(err)
		}

		if len(resp) > 0 {

			fmt.Println("Response values:")
			for _, r := range resp {
				fmt.Println(" -", r)
			}
		}
		fmt.Println()

	}

}
