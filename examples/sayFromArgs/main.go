package main

import (
	"fmt"
	"os"

	"gitlab.com/libsToGo/interfacor"
)

type Say interface {
	Hello(string) string
	Bey(string) string
}

type say struct {
}

func (s say) Hello(str string) string {
	return fmt.Sprint("Hello ", str, ", I hope all is well")
}

func (s say) Bey(str string) string {
	return fmt.Sprint("Bey ", str, ", have a good day")
}

func main() {
	var myInterface Say = &say{}

	command := os.Args[1:]

	resp, err := interfacor.ExecMethod(myInterface, command)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println(resp)

	}
}
