package interfacor

import (
	"errors"
	"fmt"
	"reflect"
	"testing"
)

type MyInterface interface {
	Method1(string)
	Method2() string
	Method3(string) string
	Method4(string) (error, string)
	Method5(int, string) (error, string)
	Method6(string) (string, error)
	notExported()
	Madness()
}

type MyStruct struct {
	val string
}

func (m *MyStruct) Method1(s string) {
	m.val = s
}

func (m *MyStruct) Method2() string {
	return m.val
}

func (m *MyStruct) Method3(s string) string {
	return s
}

func (m *MyStruct) Method4(s string) (error, string) {
	if s == "err" {
		return errors.New(s), ""
	}

	return nil, s
}

func (m *MyStruct) Method5(i int, s string) (error, string) {
	if s == "err" {
		return errors.New(s), ""
	}

	return nil, fmt.Sprint(i)
}

func (m *MyStruct) Method6(s string) (string, error) {
	if s == "err" {
		return "", errors.New(s)
	}

	return s, nil
}
func (m *MyStruct) notExported() {}

func (m *MyStruct) Madness() {
	panic("hope you catch this")
}

func TestGetInterfaceMethods(t *testing.T) {
	tests := []struct {
		input    string
		expected []string
	}{
		{"", []string{
			"Madness()",
			"Method1(string)",
			"Method2() string",
			"Method3(string) string",
			"Method4(string) (error, string)",
			"Method5(int, string) (error, string)",
			"Method6(string) (string, error)",
		}},
		{"Method", []string{
			"Method1(string)",
			"Method2() string",
			"Method3(string) string",
			"Method4(string) (error, string)",
			"Method5(int, string) (error, string)",
			"Method6(string) (string, error)",
		}},
		{"method", []string{
			"Method1(string)",
			"Method2() string",
			"Method3(string) string",
			"Method4(string) (error, string)",
			"Method5(int, string) (error, string)",
			"Method6(string) (string, error)",
		}},
		{"2", []string{"Method2() string"}},
		{"ma", []string{"Madness()"}},
	}

	myInterface := MyInterface(nil)

	for _, test := range tests {
		methods := GetMethods(&myInterface, test.input)
		if !reflect.DeepEqual(methods, test.expected) {
			t.Errorf("expected <%v> got <%v> for input <%v>", test.expected, methods, test.input)
		}
	}
}

func TestExecMethod(t *testing.T) {
	var myInterface MyInterface
	var myStruct MyStruct
	myInterface = &myStruct

	tests := []struct {
		args     []string
		err      error
		expected []interface{}
	}{
		{[]string{"Madness"}, errors.New("We panicked: hope you catch this"), nil},
		{[]string{""}, ErrMethodNotExists, nil},
		{
			[]string{"Method1", "val"},
			nil,
			[]interface{}{},
		},
		{
			[]string{"Method2"},
			nil,
			[]interface{}{"val"},
		},
		{
			[]string{"Method3", "foobar"},
			nil,
			[]interface{}{"foobar"},
		},
		{
			[]string{"Method4", "nope"},
			nil,
			[]interface{}{"nope"},
		},
		{
			[]string{"Method4", "err"},
			errors.New("err"),
			[]interface{}{},
		},
		{
			[]string{"Method5", "5", "noErr"},
			nil,
			[]interface{}{"5"},
		},
		{
			[]string{"Method5", "3", "err"},
			errors.New("err"),
			[]interface{}{},
		},
	}

	for _, test := range tests {
		val, err := ExecMethod(myInterface, test.args)
		if err != nil || test.err != nil {
			if err == nil || test.err == nil || err.Error() != test.err.Error() {
				t.Errorf("expected Err \"%v\" got Err \"%v\"", test.err, err)
			}
		}

		if !areSlicesEqual(val, test.expected) {
			t.Errorf("expected \"%v\" got \"%v\"\n", test.expected, val)
		}

	}
}

func areSlicesEqual(slice1 []reflect.Value, slice2 []interface{}) bool {
	if len(slice1) != len(slice2) {
		return false
	}

	for i, v := range slice1 {
		if !reflect.DeepEqual(v.Interface(), slice2[i]) {
			return false
		}
	}

	return true
}

func TestStringToKind(t *testing.T) {
	testCases := []struct {
		inputStr  string
		kind      reflect.Kind
		expectVal interface{}
	}{
		{"test string", reflect.String, "test string"},
		{"123", reflect.Int, int(123)},
		{"123", reflect.Int8, int8(123)},
		{"123", reflect.Int16, int16(123)},
		{"123", reflect.Int32, int32(123)},
		{"123", reflect.Int64, int64(123)},
		{"123", reflect.Uint, uint(123)},
		{"123", reflect.Uint8, uint8(123)},
		{"123", reflect.Uint16, uint16(123)},
		{"123", reflect.Uint32, uint32(123)},
		{"123", reflect.Uint64, uint64(123)},
		{"1.23", reflect.Float32, float32(1.23)},
		{"1.23", reflect.Float64, 1.23},
		{"true", reflect.Bool, true},
		// Add more test cases if necessary
	}

	for _, tc := range testCases {
		t.Run(tc.kind.String(), func(t *testing.T) {
			gotVal := stringToKind(tc.inputStr, tc.kind)
			if !reflect.DeepEqual(gotVal.Interface(), tc.expectVal) {
				t.Errorf("stringToKind(%q, %v) = %v, want %v", tc.inputStr, tc.kind, gotVal, tc.expectVal)
			}
		})
	}

	// Test the default case
	t.Run("UnsupportedType", func(t *testing.T) {
		defer func() {
			if r := recover(); r == nil {
				t.Errorf("stringToKind did not panic on unsupported type")
			}
		}()
		stringToKind("test", reflect.Slice)
	})
}
